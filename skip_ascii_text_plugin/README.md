
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Skip ASCII text](#skip-ascii-text)
  - [核心功能](#核心功能)
  - [适用环境及版本](#适用环境及版本)
  - [安装与卸载](#安装与卸载)
  - [使用与配置](#使用与配置)
  - [其他问题](#其他问题)

<!-- /code_chunk_output -->


# Skip ASCII text
## 核心功能
使用函数替换掉 calibre.utils.filename 模块中的 ascii_text 函数入口从而忽略 calibre中的ascii_text()函数，进而解决中文名称保存问题

## 适用环境及版本

- Windows10 环境下 v6.12.0 及 v6.14.1 测试可用

目前仅在Windows10系统下进行了开发及测试，其他环境的兼容性未知。

开发版本是v6.12.0，升级到最新的v6.14.1后仍生效。在github的calibre源码里确认了最早可追溯的v0.9.33版本中对应文件路径未发生过改动。

其他可能存在的问题点是使用的UI及配置代码可能在过早的版本中不受支持。

## 安装与卸载

安装步骤：

主工具栏的首选项（Preference）  
→ 高级选项（Advanced）中的插件（Plugins）  
→ 从文件里加载插件（Load plugin from file）  
→ 选择对应 skip_ascii_text_plugin.zip 压缩包  
→ 点击确认即可  
→ 如果有提示选择安装到哪个位置，选择主工具栏（main toolbar）

卸载只需把上述步骤的从文件里加载插件（Load Plugin from file） 换成移除插件（Remove plugin）即可。卸载后插件功能自动失效。

如果重新安装会继承卸载前的配置状态。卸载前是关闭状态的话需要先切换至开启状态才能生效。

## 使用与配置

加载后默认是开启状态，但安装后需要重启Calibre使插件生效。

点击主工具栏中的Skip ASCII即可查看当前插件的开启状态。  
Status of SKIP_ASCII_TEXT: ON 表示当前为开启状态，会替换掉ascii_text()函数；OFF则为关闭状态。  
开启后，导入书籍及文件均不受ascii_text()函数影响，导出到设备同理。

点击主工具栏中的Skip ASCII后再点击 Configure 进行配置。  
点击 Skip ascii_text() 前的复选框即可切换状态，勾选为开启。  
点击OK保存当前修改，点击Cancel取消当前修改。  
或者也可以在插件列表中双击 Skip ASCII Text Plugin 插件进行配置。

## 其他问题

Q1：插件本身安全么？  
A：插件压缩包其实就是把对应Python脚本文件包装起来了，不放心的可以解压出来直接看源代码（用记事本即可打开）。除了核心代码部分会对Calibre源代码有影响之外，其他代码都是无副作用的。

Q2：插件可能会有其他影响么？  
A：目前未发现有bug，影响与原本修改ascii_text()函数的方法应该是一致的，但修改ascii_text()函数这个方法具体有什么负面影响，我没进一步研究。如果发现有bug可以在评论区留言或私信沟通。

Q3：此前已导入的文件如何改成中文名称？  
A：修改函数只能影响之后的加载及导出，对此前已加载的文件没有影响。要想把此前的文件也改成中文名称，只能在插件开启的情况下导出后重新导入。  
步骤如下：选中想要重新加载的文件→保存至硬盘（save to disk）→保存至单一文件夹（save to disk in a single folder）→重新导入文件→使用Find Duplicates插件查重→按修改日期排序，删除较早修改日期的重复文件即可。  
Find duplicates插件见插件论坛页面：[https://www.mobileread.com/forums/showthread.php?t=131017](https://www.mobileread.com/forums/showthread.php?t=131017)