# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
'''
@Author  :   Billy Zhou
@Desc    :   None
'''

from importlib import reload
from calibre.gui2.actions import InterfaceAction
from calibre_plugins.skip_ascii_text.main import SkipASCIITextDialog


class SkipASCIITextPlugin(InterfaceAction):

    name = 'Skip ASCII Text Plugin'

    # Of the form: (text, icon_path, tooltip, keyboard shortcut).
    # icon, tooltip and keyboard shortcut can be None.
    action_spec = ('Skip ASCII', None,
                   'Run the Skip ASCII Text Plugin', None)

    def genesis(self):
        # This method is called once per plugin, do initial setup here

        # The qaction is automatically created from the action_spec defined
        # above
        icon = get_icons('img/icon.png', 'Skip ASCII Text')
        self.qaction.setIcon(icon)
        self.qaction.triggered.connect(self.show_dialog)

    def initialization_complete(self):
        '''
        Called once per action when the initialization of the main GUI is
        completed.
        '''
        print('Main Gui initialized. Apply SkipASCIITextPlugin settings.')
        self.apply_settings()

    def show_dialog(self):
        # The base plugin object defined in __init__.py
        base_plugin_object = self.interface_action_base_plugin
        # Show the config dialog
        # The config dialog can also be shown from within
        # Preferences->Plugins, which is why the do_user_config
        # method is defined on the base plugin class
        do_user_config = base_plugin_object.do_user_config

        # self.gui is the main calibre GUI. It acts as the gateway to access
        # all the elements of the calibre user interface, it should also be the
        # parent of the dialog
        d = SkipASCIITextDialog(self.gui, self.qaction.icon(), do_user_config)
        d.show()

    def apply_settings(self):
        from calibre_plugins.skip_ascii_text.config import prefs
        # In an actual non trivial plugin, you would probably need to
        # do something based on the settings in prefs

        from calibre.utils import filenames
        if prefs['SKIP_ASCII_TEXT']:
            def ascii_text_pass(orig):
                return orig

            filenames.ascii_text = ascii_text_pass
        else:
            reload(filenames)
