# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
'''
@Author  :   Billy Zhou
@Desc    :   None
'''

try:
    from qt.core import QDialog, QVBoxLayout, QPushButton, QLabel
except ImportError:
    from PyQt5.Qt import QDialog, QVBoxLayout, QPushButton, QLabel

from calibre_plugins.skip_ascii_text.config import prefs


class SkipASCIITextDialog(QDialog):

    def __init__(self, gui, icon, do_user_config):
        QDialog.__init__(self, gui)
        self.gui = gui
        self.do_user_config = do_user_config

        self.l = QVBoxLayout()
        self.setLayout(self.l)
        self.setWindowTitle('Skip ascii_text()')
        self.setWindowIcon(icon)

        self.status_label = QLabel('Status of SKIP_ASCII_TEXT: ' + (
            'ON' if prefs['SKIP_ASCII_TEXT'] else 'OFF'))
        self.l.addWidget(self.status_label)

        self.conf_button = QPushButton(
            'Configure', self)
        self.conf_button.clicked.connect(self.config)
        self.l.addWidget(self.conf_button)

        self.resize(self.sizeHint())

    def config(self):
        self.do_user_config(parent=self)
        self.status_label.setText('Status of SKIP_ASCII_TEXT: ' + (
            'ON' if prefs['SKIP_ASCII_TEXT'] else 'OFF'))
