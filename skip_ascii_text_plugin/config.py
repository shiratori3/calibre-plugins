# !/usr/bin/env python
# -*- encoding: utf-8 -*-
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
'''
@Author  :   Billy Zhou
@Desc    :   None
'''

try:
    from qt.core import QWidget, QVBoxLayout, QCheckBox
except ImportError:
    from PyQt5.Qt import QWidget, QVBoxLayout, QCheckBox

from calibre.utils.config import JSONConfig

prefs = JSONConfig('plugins/Skip Ascii Text Plugin')

# Set defaults
prefs.defaults['SKIP_ASCII_TEXT'] = True

# Create Widget


class ConfigWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.l = QVBoxLayout()
        self.setLayout(self.l)

        self.checkbox = QCheckBox('Skip ascii_text()')
        self.checkbox.setChecked(prefs['SKIP_ASCII_TEXT'])
        self.l.addWidget(self.checkbox)

    def save_settings(self):
        prefs['SKIP_ASCII_TEXT'] = self.checkbox.isChecked()
